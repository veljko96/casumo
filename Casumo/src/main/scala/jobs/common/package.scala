package jobs

import org.apache.spark.sql.types.{DateType, DoubleType, IntegerType, StringType, StructField, StructType, TimestampType}

package object common {

  val gameSchema = StructType(Array(
    StructField("id", StringType, nullable =true),
    StructField("name", StringType, nullable =true),
    StructField("type", StringType, nullable =true)
  ))

  val gameReviewSchema = StructType(Array(
    StructField("id", StringType, nullable =true),
    StructField("client_id", StringType, nullable =true),
    StructField("game_id", StringType, nullable =true),
    StructField("rating", IntegerType, nullable =true),
    StructField("review_timestamp", TimestampType, nullable =true),
    StructField("review_date", DateType, nullable =true)
  ))

  val stageGameReviewSchema = StructType(Array(
    StructField("id", StringType, nullable =true),
    StructField("client_id", StringType, nullable =true),
    StructField("game_id", StringType, nullable =true),
    StructField("rating", IntegerType, nullable =true),
    StructField("review_timestamp", TimestampType, nullable =true),
    StructField("review_date", DateType, nullable =true)
  ))

  val gameRatingSchema = StructType(Array(
    StructField("game_id", StringType, nullable =true),
    StructField("name", StringType, nullable =true),
    StructField("rating", DoubleType, nullable =true)
  ))

}
