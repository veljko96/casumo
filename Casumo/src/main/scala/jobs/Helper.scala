package jobs

import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, Row}

trait Helper {

  val gameReviewWindow: WindowSpec = Window
    .partitionBy("client_id", "game_id")
    .orderBy(col("review_timestamp").desc)

  def createNewStageAndFilterData(dfStageRatingGame: DataFrame,dfGameReview: DataFrame): Dataset[Row] = {
    dfGameReview.union(dfStageRatingGame)
      .withColumn("row_number", row_number().over(gameReviewWindow))
      .where(col("row_number") === 1 && col("review_date") >= add_months(current_date(), -3))
  }

  def avgRating(dfGameReview: DataFrame): DataFrame = {
    dfGameReview
      .groupBy("game_id")
      .agg(
        avg(col("rating")).as("avg_rating"),
        count(col("client_id")).as("number_of_ratings_pre_game")
      )
      .where(col("number_of_ratings_pre_game") > 5)
      .drop("number_of_ratings_pre_game")
  }

}
