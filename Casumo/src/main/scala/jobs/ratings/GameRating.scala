package jobs.ratings

import jobs.Helper
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql._

object GameRating extends App with Helper{

    val LOG: Logger = Logger.getLogger(this.getClass)

    Logger.getLogger("org").setLevel(Level.INFO)

    /**
     * 1st program argument is hive db
     */

    //If params are not passed throw exception no sense to go further
    if (args.length == 0) throw new IllegalArgumentException("Give me the params")

    val dbHive: String = args(0)
    val checkpointDirectory: String = args(1)
    val date: String = args(2)

    val spark = SparkSession.builder()
          .appName("GameRating")
          .master("local[*]")
          .enableHiveSupport()
          .getOrCreate()

    val sc = spark.sparkContext
    sc.setCheckpointDir(checkpointDirectory)

    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    import spark.sql

    val dfGame: DataFrame = sql(s"select * from $dbHive.game")
    val dfGameReview: DataFrame = sql(s"select * from $dbHive.game_review")
    val dfStageRatingGame:DataFrame = sql(s"select * from $dbHive.stage_game_rating_$date")

    dfStageRatingGame.checkpoint()
    dfGameReview.checkpoint()

    val dfFilteredNewStageGameReviews: Dataset[Row] = createNewStageAndFilterData(dfStageRatingGame,dfGameReview)

    dfFilteredNewStageGameReviews
      .write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .saveAsTable(s"$dbHive.stage_game_rating_$date")

    val calculatedAvgRatingsPerGame = avgRating(dfFilteredNewStageGameReviews)

    calculatedAvgRatingsPerGame
        .join(broadcast(dfGame), calculatedAvgRatingsPerGame("game_id") === dfGame("id"))
        .select("game_id", "name", "avg_rating")
        .write
        .format("hive")
        .mode(SaveMode.Overwrite)
        .saveAsTable(s"$dbHive.game-rating")

    spark.stop()

}
