package jobs

import java.sql.{Date, Timestamp}

import jobs.common.{gameReviewSchema, gameSchema, stageGameReviewSchema}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

class TestingReviewsOlderThanThreeMonths extends FlatSpec with Matchers with BeforeAndAfter with Helper {
    val spark = SparkSession.builder()
      .appName("TestingLastReview")
      .master("local[*]")
      .getOrCreate()

    val sc = spark.sparkContext

    "Helper" should "calculate avg rating per game" in {

      val dataGameReview: Seq[Row] = Seq(
        Row("1","1", "1", 3, Timestamp.valueOf("2021-05-07 19:54:21"), Date.valueOf("2021-05-07")),
        Row("2","2", "1", 2, Timestamp.valueOf("2021-05-07 19:55:21"), Date.valueOf("2021-05-07")),
        Row("3","1", "1", 5, Timestamp.valueOf("2021-05-07 19:59:21"), Date.valueOf("2021-05-07")),
        Row("4","1", "1", 1, Timestamp.valueOf("2021-05-07 19:51:21"), Date.valueOf("2021-05-07")),
        Row("5","2", "1", 5, Timestamp.valueOf("2021-05-07 19:32:21"), Date.valueOf("2021-06-07")),
        Row("6","1", "1", 4, Timestamp.valueOf("2021-05-07 19:11:21"), Date.valueOf("2021-06-07")),
        Row("7","2", "1", 2, Timestamp.valueOf("2021-05-07 19:59:21"), Date.valueOf("2021-06-07")),
        Row("8","2", "1", 2, Timestamp.valueOf("2021-05-07 19:52:21"), Date.valueOf("2021-06-07"))

      )

      val dataGame: Seq[Row] = Seq(
        Row("1", "League of Legends", "type_1"),
        Row("2", "DOTA", "type_2")
      )

      val stageRatingGameData: Seq[Row] = Seq(
        Row("1","1", "1", 3, Timestamp.valueOf("2021-05-06 19:54:21"), Date.valueOf("2021-05-06")),
        Row("2","2", "1", 2, Timestamp.valueOf("2021-05-06 19:55:21"), Date.valueOf("2021-06-06"))
      )

      val dfGameReview: DataFrame = spark.createDataFrame(
        sc.parallelize(dataGameReview),
        StructType(gameReviewSchema)
      )

      val dfGame: DataFrame = spark.createDataFrame(
        sc.parallelize(dataGame),
        StructType(gameSchema)
      )

      val dfStageRatingGame: DataFrame = spark.createDataFrame(
        sc.parallelize(stageRatingGameData),
        StructType(stageGameReviewSchema)
      )

      val dfFilteredGameReviews = createNewStageAndFilterData(dfStageRatingGame, dfGameReview)

      dfFilteredGameReviews.show()

      dfFilteredGameReviews.select("client_id").where("game_id like 1").count() shouldEqual 0

    }

}
